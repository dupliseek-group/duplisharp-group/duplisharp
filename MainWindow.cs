using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using Gdk;
using GLib;
using Gtk;
using Application = Gtk.Application;
using FileInfo = System.IO.FileInfo;
using UI = Gtk.Builder.ObjectAttribute;
using Window = Gtk.Window;

namespace duplisharp
{
    class MainWindow : Window
    {        
        [UI] private Button openFolder = null;
        [UI] private Button search = null;
        [UI] private ToggleButton hashCompare = null;
        [UI] private ToggleButton imageCompare = null;
        [UI] private TreeView fileTree= null;
        [UI] private Image view= null;
        [UI] private IconView duplicates = null;
        [UI] private IconView matches = null;
        [UI] private Entry threshold = null;
        [UI] private ScrolledWindow viewScrollWindow = null;

        private FileSystemTreeStore fileSystemModel = null;
        private bool loadingFolder = true;
        private Dictionary<string, List<string>> hashDict = null;

        public MainWindow() : this(new Builder("MainWindow.glade")) { }

        private MainWindow(Builder builder) : base(builder.GetObject("MainWindow").Handle)
        {                        
            builder.Autoconnect(this);               
            DeleteEvent += Window_DeleteEvent;
            openFolder.Clicked += OpenFolderClicked;
            search.Clicked += SearchClicked;
            hashCompare.SetStateFlags( hashCompare.StateFlags | StateFlags.Checked, false);
            hashCompare.Toggled += (o, args) => { Console.Out.WriteLine("hashCompare.Pressed"); };
            imageCompare.Toggled += (o, args) => { Console.Out.WriteLine("imageCompare.Pressed"); };
            
            duplicates.Model = new ImageThumbnailStore();
            duplicates.PixbufColumn = 0;    
            duplicates.SelectionChanged += DuplicatesOnSelectionChanged;
            matches.Model = new ImageThumbnailStore();
            matches.PixbufColumn = 0;
            matches.SelectionChanged += MatchesOnSelectionChanged;
            TreeViewColumn itemColumn = new TreeViewColumn ();
            itemColumn.Title = "Files";
            CellRendererText renderer = new CellRendererText ();
            itemColumn.PackStart (renderer, true);
            itemColumn.AddAttribute (renderer, "text", 0);
            itemColumn.SortColumnId = 0;
            itemColumn.SortOrder = SortType.Descending;
            itemColumn.SortIndicator = true;
            
            fileTree.Selection.Changed += TreeSelectionOnChanged;
            fileTree.AppendColumn(itemColumn);
            fileSystemModel = new FileSystemTreeStore();
            TreeModelSort sortModel = new TreeModelSort(fileSystemModel);
            sortModel.SetSortFunc(0, (model, a, b) =>
            {
                // Makes sure that folders are above files
                string d = itemColumn.SortOrder == SortType.Ascending ? "1 " : "2 ";
                string f = itemColumn.SortOrder == SortType.Ascending ? "2 " : "1 ";
                string s1 = (bool)model.GetValue(a, 2) ? d : f + (string)model.GetValue(a, 0);
                string s2 = (bool)model.GetValue(b, 2) ? d : f + (string)model.GetValue(b, 0);
                return String.Compare(s1, s2);
            });
            fileTree.Model = sortModel;
            fileTree.RowExpanded += (o, args) => fileSystemModel.FileTreeOnRowExpanded(sortModel.ConvertIterToChildIter(args.Iter));                                 
            loadingFolder = false;
        }

        private void DuplicatesOnSelectionChanged(object sender, EventArgs e)
        {
            if (duplicates.SelectedItems.Length > 0)
            {
                var treepath =  duplicates.SelectedItems[0];
                TreeIter iter;
                duplicates.Model.GetIter(out iter, treepath);
                string path = (string)duplicates.Model.GetValue(iter, 1);
                string hash = (string)duplicates.Model.GetValue(iter, 2);
                Rectangle rect = viewScrollWindow.Allocation;
                
                view.Pixbuf = new Pixbuf(path,rect.Width, rect.Height);
                
                var imageMatches = hashDict[hash];
                List<Tuple<string, string>> images = new List<Tuple<string, string>>(); 
                foreach (var imageMatch in imageMatches)
                {
                    images.Add(Tuple.Create(imageMatch, ""));
                }
                ((ImageThumbnailStore) matches.Model).Images = images.ToArray();
                SelectFileInTree(path);
            }
        }

        private void MatchesOnSelectionChanged(object sender, EventArgs e)
        {
            if (matches.SelectedItems.Length > 0)
            {
                var treepath =  matches.SelectedItems[0];
                TreeIter iter;
                matches.Model.GetIter(out iter, treepath);
                string path = (string)matches.Model.GetValue(iter, 1);    
                Rectangle rect = viewScrollWindow.Allocation;
                view.Pixbuf = new Pixbuf(path,rect.Width, rect.Height);
                SelectFileInTree(path);                
            }
        }

        void SelectFileInTree(string path)
        {
            TreeIter iter = GetFileIterFromPath(path);
                  
            iter = ((TreeModelSort) fileTree.Model).ConvertChildIterToIter(iter);
            TreePath treePath = fileTree.Model.GetPath(iter);
            fileTree.CollapseAll();
            fileTree.ExpandToPath(treePath);
            fileTree.Selection.SelectIter(iter);
            fileTree.ScrollToCell(treePath, fileTree.GetColumn(0), true, 0, 0);
        }

        private TreeIter GetFileIterFromPath(string path)
        {
            var paths = path.Replace(fileSystemModel.Path + System.IO.Path.DirectorySeparatorChar, "").Split( System.IO.Path.DirectorySeparatorChar);
            TreeIter? iter = null;
            foreach (var p in paths)
            {
                iter = FindIter(iter, p);
                if ((bool)fileSystemModel.GetValue(iter.Value, 2))
                    fileSystemModel.FileTreeOnRowExpanded(iter.Value);    
            }                                              
            return iter.Value;
        }

        TreeIter? FindIter(TreeIter? parentIter, string name)
        {
            TreeIter childIter;
            if (!parentIter.HasValue) 
                fileSystemModel.IterChildren(out childIter);
            else
                fileSystemModel.IterChildren(out childIter, parentIter.Value);
            bool cc = true;
            do
            {
                string child_name = (string)fileSystemModel.GetValue(childIter, 0);
                if (child_name == name) return childIter;
                cc = fileSystemModel.IterNext(ref childIter);
            } while (cc);

            return null;
        }

        private void TreeSelectionOnChanged(object sender, EventArgs e)
        {
            if (loadingFolder) return;
            TreeIter iter;
            fileTree.Selection.GetSelected(out iter);
            Value value = new Value();
            fileTree.Model.GetValue(iter, 1, ref value);
            if (File.Exists((string) value.Val))
            {                
                Rectangle rect = viewScrollWindow.Allocation;
                view.Pixbuf = new Pixbuf((string) value.Val,rect.Width, rect.Height);
            }
        }

        private void Window_DeleteEvent(object sender, DeleteEventArgs a)
        {
            Application.Quit();
        }

        private void OpenFolderClicked(object sender, EventArgs a)
        {
            FileChooserDialog fcd = new FileChooserDialog("Select folder to search", this, FileChooserAction.SelectFolder);
            fcd.AddButton (Gtk.Stock.Cancel, Gtk.ResponseType.Cancel);
            fcd.AddButton (Gtk.Stock.Open, Gtk.ResponseType.Ok);
            fcd.DefaultResponse = Gtk.ResponseType.Ok;
            int response = fcd.Run ();
            if (response == (int)ResponseType.Ok)
            {
                string folder = fcd.Filename;
                loadingFolder = true;
                fileSystemModel.Path = folder;
                loadingFolder = false;
                hashDict = null;
            }
            fcd.Dispose();            
        }
        
        private void SearchClicked(object sender, EventArgs a)
        {            
            hashDict = ImageCompare.FindHashMatches(fileSystemModel.Path);
            List<Tuple<string, string>> images = new List<Tuple<string, string>>(); 
            if ((imageCompare.StateFlags & StateFlags.Checked) == StateFlags.Checked)
            {
                double threshold_val = double.Parse(threshold.Text);
                ImageCompare.FindImageMatches(hashDict, threshold_val);
            }
            foreach (var hashlist in hashDict)
            {
                if (hashlist.Value.Count > 1)
                {
                    images.Add(Tuple.Create(hashlist.Value[0], hashlist.Key));        
                }
            }            
            ((ImageThumbnailStore) duplicates.Model).Images = images.ToArray();
            
        }       
    }
}
