﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Security.Cryptography;
using System.Linq;
using System.Threading;
using Gdk;
using GLib;
using FileInfo = System.IO.FileInfo;
using Thread = System.Threading.Thread;

namespace duplisharp
{
    public class ThumbCompareWorker
    {
        public static double threshold = 1.0; 
        public static ConcurrentStack<string> exclusionList;
        public static Dictionary<string, float[]> thumbDict = null;
        public static Dictionary<string, List<string>> hashDict = null;
        public int offset = 0;
        public static int step = 1;

        public ThumbCompareWorker(Dictionary<string, float[]> new_thumbDict,
            Dictionary<string, List<string>> new_hashDict, int new_offset)
        {
            exclusionList = new ConcurrentStack<string>();
            offset = new_offset;
            hashDict = new_hashDict;
            thumbDict = new_thumbDict;
        }
        
        public void Run()
        {            
            var hashList = hashDict.Keys.ToArray();
            int count = hashList.Length;
            Console.Out.WriteLine("worker started with offset: " + offset.ToString());
            for (int i = offset; i < count; i+=step)
            {
                for (int j = i+1; j < count; j++)
                {
                    double diff = 0;
                    string hasha = hashList[i];
                    string hashb = hashList[j];
                    if (!thumbDict.ContainsKey(hasha) || !thumbDict.ContainsKey(hashb))
                        Console.Out.WriteLine(hasha + " not found");
                    float[] a = thumbDict[hasha];
                    float[] b = thumbDict[hashb];
                    if (a != null && b != null && !exclusionList.Contains(hasha))
                    {
                        for (int m = 0; m< a.Length; m++)
                        {
                            diff += Math.Abs(a[m] - b[m]);                            
                        }  
                        // if difference is less than threshold match is added to hashDict
                        double diffPercentage = 100*(diff / (255*ImageCompare.cw * ImageCompare.ch));
                        if (diffPercentage < threshold)
                        {
                            hashDict[hasha].Add(hashDict[hashb][0]);
                            exclusionList.Push(hashb);
                        } 
                    }                                                         
                }
            }
            Console.Out.WriteLine("worker ended");
        }
    }

    public class ImageLoaderWorker
    {
        public Stack<Tuple<string, string>> imagePathBuffer = new Stack<Tuple<string, string>>();
        public Stack<Tuple<float[], string>> imageBuffer = new Stack<Tuple<float[], string>>();
                
        public AutoResetEvent waitHandle = new AutoResetEvent(false);
        public void Run()
        {
            while (imagePathBuffer.Count>0)
            {                
                Tuple<string,string> pathhash;
                if (imagePathBuffer.TryPop(out pathhash))
                {                    
                    imageBuffer.Push(Tuple.Create(LoadImage(pathhash.Item1), pathhash.Item2));                    
                    //waitHandle.Set();
                }
                else
                {
                    Thread.Sleep(10);
                }
            }                            
            Console.Out.WriteLine("thread dead");
        }
        
        public static float[] LoadImage(string path)
        {
            try
            {
                Pixbuf pixbuf = new Pixbuf(path, ImageCompare.cw, ImageCompare.ch, false);
                return Pixbuf2BWArray(pixbuf);
            }
            catch (GException e)
            {
                
            }

            return null;

        }
        public static float[] Pixbuf2BWArray(Pixbuf pb)
        {
            int count = pb.Width * pb.Height;  
            float[] array = new float[count];
            byte[] data = pb.PixelBytes.Data;
            for (int i = 0; i < count; i += pb.HasAlpha ? 3 : 4)
            {
                float R = data[i];
                float G = data[i+1];
                float B = data[i+2];
                float Y = 0.3f * R + 0.59f * G + 0.11f * B;
                array[i] = Y;
            }
            return array;
        }
    }
    
    public class ImageCompare
    {
        public const int cw = 200;
        public const int ch = 200;
        public static readonly string[] fileTypeFilter = {".png", ".jpg", ".tiff", ".tga" };               
        public Dictionary<string, List<string>> hashDict = new Dictionary<string, List<string>>();
        public static string ImageHash(string filename)
        {
            using (MD5 md5 = MD5.Create())
            {
                using (FileStream stream = File.OpenRead(filename))
                {
                    byte[] hash = md5.ComputeHash(stream);
                    return BitConverter.ToString(hash).Replace("-", "").ToLowerInvariant();
                }
            }
        }

        public static Dictionary<string, List<string>> FindHashMatches(string path)
        {            
            ImageCompare imgcmp = new ImageCompare();
            imgcmp.ProcessDirectory(path);
            return imgcmp.hashDict;
        }
        
        public void ProcessDirectory(string targetDirectory)
        {
            // Process the list of subdirs found in the directory.
            string[] subdirectoryEntries = Directory.GetDirectories(targetDirectory);
            foreach (string subdirectory in subdirectoryEntries)
            {                
                ProcessDirectory(subdirectory);
            }

            // Process the list of files found in the directory.
            string[] fileEntries = Directory.GetFiles(targetDirectory);
            foreach (string fileName in fileEntries)
            {
                ProcessFile(fileName);                
            }

            Debug.WriteLine("Test");
        }
        public void ProcessFile(string path)
        {
            FileInfo file = new FileInfo(path);
            if (!fileTypeFilter.Contains(file.Extension)) return;
            string hash = ImageCompare.ImageHash(path);
            Debug.WriteLine(path + ": " + hash);

            if (hashDict.ContainsKey(hash))
            {
                hashDict[hash].Add(path);
            }
            else
            {
                List<string> filePaths = new List<string>();
                hashDict.Add(hash, filePaths);
                hashDict[hash].Add(path);
            }
        }

        public static void FindImageMatches(Dictionary<string, List<string>> hashDict, double threshold)
        {
            var hashList = hashDict.Keys.ToArray();
            var count = hashList.Length;
            Dictionary<string, float[]> thumbDict = new Dictionary<string, float[]>();
            int threadCount = Math.Min(Environment.ProcessorCount, hashList.Length);
            Thread[] threads = new Thread[threadCount];
            ImageLoaderWorker[] workers = new ImageLoaderWorker[threadCount];
            int image_counter = 0;
            Console.Out.WriteLine("Loading images");
            for (int i = 0; i < threadCount; i++)
            {                
                ImageLoaderWorker worker = new ImageLoaderWorker();
                workers[i] = worker;
                string hash = hashList[image_counter];
                string path = hashDict[hash][0];
                image_counter++;
                worker.imagePathBuffer.Push(Tuple.Create(path, hash));
                threads[i] = new Thread(worker.Run);
                
            }

            do
            {                
                for (int i = 0; i < threadCount; i++)
                {
                    var worker = workers[i];                    
                    if (image_counter < hashList.Length)
                    {
                        string hash = hashList[image_counter];
                        string path = hashDict[hash][0];
                        image_counter++;
                        worker.imagePathBuffer.Push(Tuple.Create(path, hash));
                    }                         
                }
            } while (image_counter < hashList.Length);


            for (int i = 0; i < threadCount; i++)
            {
                threads[i].Start();
            }
            for (int i = 0; i < threadCount; i++)
            {
                threads[i].Join();
            }
            for (int i = 0; i < threadCount; i++)
            {
                var worker = workers[i];                
                while (worker.imageBuffer.Count>0)
                {                    
                    var imghash = worker.imageBuffer.Pop();
                    thumbDict[imghash.Item2] = imghash.Item1;                                       
                }
            }
            Console.Out.WriteLine("Calculating difference between thumbnails");
            // Calculating difference between thumbnails 
            ThumbCompareWorker[] tworkers = new ThumbCompareWorker[threadCount];
            for (int i = 0; i < threadCount; i++)
            {                   
                ThumbCompareWorker.step = threadCount;
                ThumbCompareWorker.threshold = threshold;
                ThumbCompareWorker worker = new ThumbCompareWorker(thumbDict,hashDict, i);
                tworkers[i] = worker;
                worker.offset = i;                                
                threads[i] = new Thread(worker.Run);                
            }
            for (int i = 0; i < threadCount; i++)
            {
                threads[i].Start();
            }
            for (int i = 0; i < threadCount; i++)
            {
                threads[i].Join();
            }
            
            Console.Out.WriteLine("compare done");
        }        
    }
}
