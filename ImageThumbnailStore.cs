using System;
using Cairo;
using Gdk;
using GLib;
using Gtk;

namespace duplisharp
{
    public class ImageThumbnailStore : ListStore
    {
        private Tuple<string, string>[] images = new Tuple<string, string>[0];
        public ImageThumbnailStore() : base(typeof(Pixbuf), typeof(string), typeof(string))
        {                        
            Populate();
        }

        public Tuple<string, string>[] Images
        {
            get => images;
            set
            {
                images = value;
                Clear();
                Populate();
            }
        }

        private void Populate()
        {
            foreach (var image in Images)
            {
                try
                {
                    string path = image.Item1;
                    string hash = image.Item2;
                    Pixbuf pixbuf = new Pixbuf(path, 200, 200);
                    AppendValues(pixbuf, path, hash);
                }
                catch (GException e)
                {
                    Console.Out.WriteLine("Cant show: " + image.Item1);
                }
            }             
        }
    }
}
