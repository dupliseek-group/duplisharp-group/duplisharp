using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using GLib;
using Gtk;
using FileInfo = System.IO.FileInfo;

namespace duplisharp
{
    public class FileSystemTreeStore : TreeStore
    {             
        private string path = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures);
        public FileSystemTreeStore() : base(GType.String, GType.String, GType.Boolean)
        {
            Populate();
        }

        private void Populate()
        {                                  
            ProcessDirectory(path, null);            
        }
        
        public string Path
        {
            get { return path; }
            set
            {
                if (Directory.Exists(value))
                {
                    path = value;
                    Clear();
                    Populate();
                } 
            }
        }

        public void ProcessDirectory(string targetDirectory, TreeIter? parent)
        {
            // Process the list of subdirs found in the directory.
            string[] subdirectoryEntries = Directory.GetDirectories(targetDirectory);
            foreach (string subdirectory in subdirectoryEntries)
            {
                DirectoryInfo dir = new DirectoryInfo(subdirectory);
                TreeIter iter;
                if (parent.HasValue)
                    iter = AppendValues(parent.Value, dir.Name, subdirectory, true);
                else
                    iter = AppendValues(dir.Name, subdirectory, true);
                // Add Dummy child so the expand button appears in the view
                AppendValues(iter, "dummy", "dummy");                
            }

            // Process the list of files found in the directory.
            string[] fileEntries = Directory.GetFiles(targetDirectory);
            foreach (string fileName in fileEntries)
            {
                ProcessFile(fileName, parent);
            }            
        }
        public void ProcessFile(string path, TreeIter? parent)
        {
            FileInfo file = new FileInfo(path);
            if (!ImageCompare.fileTypeFilter.Contains(file.Extension)) return;
                                      
            if (parent.HasValue)
                AppendValues(parent.Value, file.Name, path, false);
            else AppendValues(file.Name, path, false);
        }
        
        public void FileTreeOnRowExpanded(TreeIter rowIter)
        {
            string path = (string)GetValue(rowIter, 1);
            TreeIter child;
            
            int childCount = IterNChildren(rowIter);
            if (childCount == 0) return; // this means that the folder is empty
            IterChildren(out child, rowIter);
            
            if ((string) GetValue(child, 1) != "dummy") return; // this means that the node is already populated
                        
            ProcessDirectory(path, rowIter);
            Remove(ref child); // removes dummy child
        }
        
        public void FileTreeOnRowCollapsed(object o, RowCollapsedArgs args)
        {
            
        }
    }
}